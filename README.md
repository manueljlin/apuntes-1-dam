# Apuntes 1 DAM

Por ahora solo tiene los apuntes de MySQL que compartí via Notion en la clase, aunque también iré añadiendo de PHP etc.

Para acceder a los apuntes en sí y no al repositorio [haz clic aquí](https://manueljlin.gitlab.io/apuntes-1-dam/).

[TOC]

## ¿Cómo puedo ayudar a corregir problemas/añadir más contenido?

¡Me alegro mucho de que quieras ayudar!

Si tienes una cuenta en GitLab puedes crear un issue con lo que quieras modificar y lo cambiaré yo a mano.

Si no te quieres hacer una cuenta porque prefieres GitHub o sencillamente no quieres luchar con esto (te comprendo jajaja), envíame un mensaje por el grupo o por privado con lo que quieras cambiar.


## ¡Pero espera! ¡Soy la hostia y quiero hacer merge requests/pull requests!

¡Perfecto! Esta es una pequeña guía de pasos a seguir para hacerlo tú mismo. Anímo a ir probando Git con estos pasos incluso si no quieres subir nada, tener esta base ayudará mucho cuando empecemos a hacer los proyectos grupales.

Es solo una de las varias formas de hacerlo. Si prefieres usar una interfaz al estilo GitHub Desktop, por supuesto, adelante.

Esto asume que tienes Git instalado en tu ordenador. Si no, descárgalo en https://git-scm.com/downloads

---


### Configurando Git

Esto parece mucha cosa pero tranquilo, solo hace falta hacerlo una vez por ordenador.

1. Primero, abre un terminal.
    - Si usas Windows como en clase, recomiendo que uses el de  `Git Bash` porque tiene soporte para autocompletar al pulsar tab.
    - Si usas macOS o Linux, da igual el que uses.
2. Ahora, tienes que poner tu nombre y correo con el que firmarás los cambios.
    ```bash
    git config --global user.name 'Pepito'
    git config --global user.email 'pepito@gmail.com'
    ```
3. Genera una llave SSH con el siguiente comando. Esto te permite conectar tu cuenta con el repositorio, a grandes rasgos.

    ```
    ssh-keygen -t ed25519 -C 'Mi Clave'
    ```
    Puedes reemplazar el texto entre comillas por lo que quieras.

    Te va a pedir el directorio donde lo va a guardar. Solo pulsa Intro y ya está. Luego, en la contraseña, pon cualquier contraseña que quieras.

4. El paso de antes ha generado unos archivo en ~/.ssh/ que tenemos que añadir a nuestra cuenta.

    - Nota: Cuando un directorio pone ~/ significa que es un subdirectorio en la carpeta de tu usuario. En Windows es C:\Users\nombre_usuario

    1. Abre ese directorio
    2. Tendrás dos archivos: `id_ed25519` y `id_ed25519.pub`. El primero es lo que se llama la llave privada, que no debes dar a nadie. El segundo es el público, el que tienes que subir.
    3. Abre id_ed25519.pub con el bloc de notas, Visual Studio Code etc y copia los contenidos. Tendrá un formato similar a este:
        ```
        ssh-ed25519 ASDFasGFSDAGGf Mi Clave 
        ```
    4. Añádelos en https://gitlab.com/-/profile/keys

¡Ya está! Puedes aprovechar y ponerlo también en GitHub si quieres: https://github.com/settings/ssh/new

---


### Clonando el repositorio y añadiendo tus propios contenidos

#### Clonando el repositorio

1. Crea un fork con el botón de arriba de la página.
2. Clona el repositorio
3. En la página de tu fork, pulsa el botón azul que pone Clone y copia entero en enlace que empieza por git@gitlab.com
4. Abre el mismo terminal de antes.

    Te va a preguntar si te fías de la conexión con GitLab, escribe `yes`

    Por último, te va a pedir la contraseña que escribiste antes. Escríbela y pulsa intro.

#### Añade tu propio contenido

1. Cambia todos los archivos que hagan falta.
2. Crea una rama con `git checkout -b nombre_rama` siendo nombre_rama algo como apuntes_php por ejemplo. 
3. Añade los archivos con `git add .`
4. Crea un commit con `git commit -m 'Añadiendo apuntes PHP'` o con cualquier otro título.
5. Haz un push a tu fork con `git push origin main`. Escribe tu contraseña y pulsa intro.
    
    Dependiendo de como hayas hecho los cambios, puede que sea distinto y sea fork en vez de main, pero si has seguido los pasos como lo he escrito funciona.

6. Crea un merge request en la página de tus commits.

---

#### Probando los cambios en MkDocs

Esto es solo por si quieres ver una vista previa mientras vas modificando los archivos.

1. Instala Python
2. Instala Material MkDocs con `pip install mkdocs-material`
3. Entra al directorio raíz del repositorio
4. Abre un terminal y escribe `mkdocs serve`
5. Abre http://localhost:8000
