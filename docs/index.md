# Inicio

Bienvenido a la página de apuntes de nuestro curso. Selecciona la asignatura en el menú de navegación, a la izquierda en el móvil y en la barra de navegación superior en el ordenador.

Está _muy_ incompleto y medio roto ahora mismo, pero va a ser muy útil conforme vaya avanzando el curso.


## Cómo ayudar con este proyecto

Si quieres ayudar a corregir errores, o incluyo añadir tus propios fragmentos de ejemplos, explicaciones etc tienes varias opciones:

- Contármelo de forma directa: Telegram, WhatsApp o el grupo de Discord
- Añadir un issue [en el propio repositorio](https://gitlab.com/manueljlin/apuntes-dam/-/issues/new)
- Crear un merge request [haciendo un fork del repositorio](https://gitlab.com/manueljlin/apuntes-dam) siguiendo las instrucciones del README.md <3