# Constantes, variables, tipos de datos y matrices
Una adaptación corta del primer set de ejemplos del Classroom.

[TOC]


## Constantes
Las constantes son un identificador para un valor simple. Son diferentes a las variables normales en algunos aspectos:

- No se puede cambiar su valor otra cosa mientras se ejecuta el script.
- El nombre de las constantes deben escribirse siempre en mayúscula.
- No hace falta usar $ antes del nombre.

```php
// Definimos las constantes
define('FOO',       'cosa');
define('FOO_BAR',   'otra cosa');
define('ANIMALES',  array('perro', 'gato', 'pájaro'));

// También se puede definir con const
const FOO_BAR2   =  'foo bar 2 electric boogaloo';
const COLOR      =  array('rojo', 'verde', 'azul');

// Lo usamos en otra parte del código
echo FOO,'<br>';
echo ANIMALES[2],'<br>';
echo COLOR[0],'<br>';
```
```php
// Output en página web
cosa
pájaro
rojo
```


## Variables

### Inicialización y asignación de una variable
Las variables se usan para guardar y leer información para usarse en un programa. Hay varios tipos a poder declarar como array, bool, float, int, string, object etc.

```php
// Inicializamos la variable $nombre
$nombre = 'Justin Bieber';

// Mostramos la variable $nombre
echo $nombre,'<br>';


// Cambiar el tipo de string a int reasignando el valor a un número entero
$nombre = 123;

// Mostramos la variable $nombre
echo $nombre,'<br>';
```
```php
// Output en página web
Justin Bieber
123
```


### Variables dinámicas
También llamadas _variables variables_, son variables que tienen como valor el valor de otra variable. Es decir, **una variable tiene el nombre de otra segunda variable como valor**, que al poner dos $$ delante lee como variable en vez de string.

```php
// Inicializamos las variables. Usamos ciudades y el n. de habitantes
$Linares = 57811
$Jaen    = 113457

$nombreciudad = 'Linares';
echo "El nº de habitantes en $nombreciudad es ${$nombreciudad}",'<br>';

$nombreciudad = 'Jaen';
echo "El nº de habitantes en $nombreciudad es ${$nombreciudad}",'<br>';
```
```php
// Output en página web
El nº de habitantes en Linares es 57811
El nº de habitantes en Jaen es 113457
```

Esto no está limitado a dos, aquí otro ejemplo de los apuntes:

```php
// Inicializamos las variables.
$Bar = "a";
$Foo = "Bar";
$World = "Foo";
$Hello = "World";
$a = "Hello";

echo $a.'<br/>'; //devuelve Hello
echo $$a.'<br/>'; //devuelve World
echo $$$a.'<br/>'; //devuelve Foo
echo $$$$a.'<br/>'; //devuelve Bar
```


## Tipos de datos

### Conversión de una cadena en un número
PHP puede convertir un string a un int o float para p. ej realizar operaciones aritméticas.

```php
echo '1 + "1" = ',(1 + "1"),'<br>';
```
```php
// Output en página web
1 + "1" = 2
```

## Arrays (matrices)

### Acceder a un elemento individual de un array
```php
$números = array('cero','uno','dos','tres',5 => 'cinco','seis','uno' => 1,'siete',-1 => 'menos uno');
echo $números[1],'<br>';     // Output: uno
echo $números['uno'],'<br>'; // Output: 1
```
### Examinar un array

```php
// Inicialización de un array
$números = array('cero','uno','dos',
                'cero' => 0,'uno' => 1,'dos' => 2);

// Examinar la matriz con la primera sintaxis.
echo 'Primera sintaxis:','<br>';
foreach($números as $número) {
    echo "$número",'<br>';
}

// Examinar la matriz con la segunda sintaxis.
echo 'Segunda sintaxis:','<br>';
foreach($números as $clave => $número) {
    echo "$clave => $número<br />";
}


// Inicialización de una matriz con dos dimensiones.
$capitales = [['ESPAÑA','Madrid'],['ITALIA','Roma']];

// Examinar la matriz con foreach y list.
echo 'Tercera sintaxis:','<br>';
foreach ($capitales as list($país,$ciudad)) {
    echo "$país: $ciudad<br />";
}
```