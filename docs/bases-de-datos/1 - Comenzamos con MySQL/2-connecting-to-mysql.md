---
title: Conectarse a la base de datos 
---


# Conectándose a la base de datos de MySQL

Para conectarte a la base de datos y ejecutar los comandos, primero tienes que iniciarla con alguna de estas herramientas: 


## Iniciando el servidor de la base de datos

=== "Usando Laragon en Windows"
    1. Abre Laragon
    2. Haz clic en el logo del Laragon 🐘, o el botón "Start All"

    !!! attention "Si no funciona, comprueba que el directorio data y el puerto 3306 de MySQL en los ajustes de Laragon estén configurados correctamente"

=== "Usando XAMPP en macOS"
    1. Abre XAMPP Control
    2. Inicia Apache, MySQL y ProFTPD

=== "Usando XAMPP en Linux"

    ### Vía terminal
    Abre XAMMP con `sudo /opt/lampp/lampp start`

    ### Vía GUI
    1. `cd /opt/lampp`
    2. `sudo ./manager-linux.run (o manager-linux-x64.run)`


## Iniciando sesión en el cliente

Ahora que tienes MySQL funcionando en el fondo, puedes conectarte abriendo un terminal y escribiendo el siguiente comando:

``` title="Shell del sistema"
mysql -u root -p
```
Te pedirá una contraseña. Si no tienes niguna, solamente pulsa ++enter++ de nuevo y se conectará.


## Cerrando cliente y servidor

Para cerrar el cliente, escribe el siguiente comando:

```mysql title="Shell de MySQL"
exit;
```

!!! info "Nota: también puedes usar `quit` o `\q`"

Para cerrar el servidor, abre la ventana de Laragon o XAMPP y detén los servicios de la misma forma que lo has iniciado.