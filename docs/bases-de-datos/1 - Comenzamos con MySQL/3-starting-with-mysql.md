---
title: Comenzando con MySQL 
---

# Primeros pasos con MySQL

MySQL gestiona varias bases de datos. A través de una serie de comandos de SQL, puedes acceder y filtrar esa información.


## Importando una base de datos

En el caso de que necesites añadir un archivo `.SQL` que te haya dado Raúl en clase, se agregaría con un comando similar al de iniciar sesión:

=== "Usando Windows"

    ``` title="Shell del sistema"
    mysql -u root -p < C:\Users\nombre_usuario\...
    ```

=== "Usando macOS o Linux"

    ``` title="Shell del sistema"
    mysql -u root -p < ~/ruta/al/archivo.sql
    ```

!!! info "En ambos casos, puedes evitar escribir la ruta del archivo a mano arrastrándo al Terminal"


## Mostrar todas las bases de datos disponibles

Para acceder a una base de datos, primero tienes que seleccionarla. Muestra todas las bases de datos disponibles con el comando siguiente:

```mysql title="Shell de MySQL"
show databases;


# Ejemplo del resultado del comando

+--------------------+
| Database           |
+--------------------+
| claseonline        |
| tiendaonline       |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
```

Luego, puedes seleccionar uno de la lista con `#!mysql use claseonline;`


## Tablas

Dentro de una base de datos hay varias tablas con contenido. Para mostrar las tablas de una base de datos, escribe `#!mysql show tables;`